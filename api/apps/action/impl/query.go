package impl

import (
	"gitee.com/talk-about-articles/workflow/api/apps/action"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// 构造请求和数据校验

type queryRequest struct {
	*action.QueryActionRequest
}
type describeRequest struct {
	*action.DescribeActionRequest
}
type deleteRequest struct {
	*action.DeleteActionRequest
}
type updateRequest struct {
	*action.UpdateActionRequest
}

func NewQueryRequest(req *action.QueryActionRequest) (*queryRequest, error) {

	if err := req.Validate(); err != nil {

		return nil, err

	}

	// 构建一个QueryRequest对象
	return &queryRequest{req}, nil

}

func NewDeleteRequest(req *action.DeleteActionRequest) (*deleteRequest, error) {

	if err := req.Validate(); err != nil {

		return nil, err

	}

	return &deleteRequest{req}, nil

}

func NewDescribeRequest(req *action.DescribeActionRequest) (*describeRequest, error) {

	if err := req.Validate(); err != nil {
		return nil, err
	}

	return &describeRequest{req}, nil

}

func (r *queryRequest) FindOptions() *options.FindOptions {
	pageSize := int64(r.Page.PageSize)
	skip := int64(r.Page.PageSize) * int64(r.Page.PageNumber-1)

	opt := &options.FindOptions{
		// 排序： Order By create_at Desc
		Sort: bson.D{
			{Key: "create_at", Value: -1},
		},
		Limit: &pageSize,
		Skip:  &skip,
	}

	return opt
}

func (r *queryRequest) QueryFilter() bson.M {
	filter := bson.M{}

	if r.Name != "" {
		filter["name"] = r.Name
	}

	if r.Namespace != "" {
		filter["namespace"] = r.Namespace
	}

	return filter
}

func (r *describeRequest) DescribeFilter() bson.M {

	filter := bson.M{}
	filter["name"] = r.Name
	filter["version"] = r.Version

	return filter
}

func (r *deleteRequest) DeleteFilter() bson.M {

	filter := bson.M{}

	filter["name"] = r.Name
	filter["version"] = r.Version
	filter["namespace"] = r.Namespace

	return filter

}
