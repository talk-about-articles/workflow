package impl

import (
	"context"
	"gitee.com/talk-about-articles/workflow/api/apps/action"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

func (s *service) CreateAction(ctx context.Context, req *action.CreateActionRequest) (*action.Action, error) {

	a, err := action.NewAction(req)
	if err != nil {

		return nil, err

	}
	if _, err := s.col.InsertOne(context.TODO(), a); err != nil {

		return nil, exception.NewInternalServerError("insert error,%s", err)
	}

	return a, err

}
func (s *service) QueryAction(ctx context.Context, req *action.QueryActionRequest) (*action.ActionSet, error) {
	qr, err := NewQueryRequest(req)
	if err != nil {

		return nil, exception.NewInternalServerError("validate Action data error , %s", err)

	}
	set := action.NewActionSet()
	resp, err := s.col.Find(context.TODO(), qr.QueryFilter(), qr.FindOptions())

	for resp.Next(ctx) {
		ins := action.NewDefaultAction()
		if err := resp.Decode(ins); err != nil {
			return nil, exception.NewInternalServerError("decode action error, %s", err)
		}
		set.Add(ins)
	}
	// count
	count, err := s.col.CountDocuments(context.TODO(), qr.QueryFilter())
	if err != nil {
		return nil, exception.NewInternalServerError("get action count error")
	}
	set.Total = count
	return set, nil
}
func (s *service) DescribeAction(ctx context.Context, req *action.DescribeActionRequest) (*action.Action, error) {

	// 构建一个对象过滤条件的基础类
	des, err := NewDescribeRequest(req)
	if err != nil {

		return nil, exception.NewInternalServerError("validate describe data error, %s", err)

	}

	result := s.col.FindOne(context.TODO(), des.DescribeFilter())
	ins := action.NewDefaultAction()
	if err := result.Decode(ins); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("action not found ")
		}
		return nil, err
	}

	return ins, err

}
func (s *service) UpdateAction(ctx context.Context, req *action.UpdateActionRequest) (*action.Action, error) {

	if err := req.Validate(); err != nil {

		return nil, exception.NewBadRequest("validate updateAction error , %s ", err)
	}
	ins, err := s.DescribeAction(ctx, action.NewDescribeActionRequest(req.Name, req.Version))
	if err != nil {
		return nil, err
	}
	ins.Update(req)
	ins.UpdateAt = time.Now().UnixMilli()
	_, err = s.col.UpdateOne(context.TODO(), bson.M{"name": req.Name, "version": req.Version}, bson.M{"$set": ins})
	if err != nil {

		return nil, exception.NewInternalServerError("update action %s error,%s", ins.Key(), err)

	}
	return ins, nil

}
func (s *service) DeleteAction(ctx context.Context, req *action.DeleteActionRequest) (*action.Action, error) {

	// 查询出当前的要删除的对象
	del_ins, err := s.DescribeAction(ctx, action.NewDescribeActionRequest(req.Name, req.Version))
	if err != nil {

		return nil, exception.NewInternalServerError("get delete action error , %s", err)

	}

	del, err := NewDeleteRequest(req)
	if err != nil {
		return nil, exception.NewInternalServerError("validate delete data error , %s", err)
	}
	result, err := s.col.DeleteOne(context.TODO(), del.DeleteFilter())
	if err != nil {
		return nil, exception.NewInternalServerError("action delete error ,  %s ")
	}
	if result.DeletedCount == 0 {
		return nil, exception.NewNotFound("action delete error or not found")
	}
	return del_ins, nil

}
