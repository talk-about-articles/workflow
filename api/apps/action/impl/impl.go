package impl

import (
	"context"
	"gitee.com/talk-about-articles/workflow/api/apps/action"
	"gitee.com/talk-about-articles/workflow/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"google.golang.org/grpc"
)

var (
	// Service 服务实例
	svr = &service{}
)

type service struct {
	col *mongo.Collection
	log logger.Logger
	action.UnimplementedServiceServer
}

func (s *service) Config() error {
	// 依赖Mongodb的DB对象
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}

	// 获取一个Collection对象，通过collection对象完成数据库操作
	s.col = db.Collection(s.Name())
	s.log = zap.L().Named(s.Name())

	// 创建索引
	indexs := []mongo.IndexModel{
		{
			Keys: bsonx.Doc{
				{
					Key: "name", Value: bsonx.Int32(-1),
				},
				{
					Key: "version", Value: bsonx.Int32(-1),
				},
			},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys: bsonx.Doc{
				{
					Key: "create_at", Value: bsonx.Int32(-1),
				},
			},
		},
	}
	_, err = s.col.Indexes().CreateMany(context.Background(), indexs)
	if err != nil {
		return err
	}

	return nil
}

func (s *service) Name() string {
	return action.AppName
}

func (s *service) Registry(server *grpc.Server) {
	action.RegisterServiceServer(server, svr)
}

func init() {
	app.RegistryGrpcApp(svr)
}
