package http

import (
	"gitee.com/talk-about-articles/workflow/api/apps/action"
	"gitee.com/talk-about-articles/workflow/common/page"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/http/response"
)

func (h *handler) CreateAction(c *gin.Context) {

	req := action.NewCreateActionRequest()
	if err := c.Bind(req); err != nil {

		response.Failed(c.Writer, err)
		return

	}

	req.UpdateOwner()

	ins, err := h.service.CreateAction(c.Request.Context(), req)
	if err != nil {
		response.Failed(c.Writer, err)
		return
	}

	response.Success(c.Writer, ins)

}

func (h *handler) QueryAction(c *gin.Context) {

	page := page.NewPageRequestFromHTTP(c.Request)
	req := action.NewQueryActionRequest()
	req.Page = page
	req.Namespace = "default"

	set, err := h.service.QueryAction(c.Request.Context(), req)
	if err != nil {
		response.Failed(c.Writer, err)
	}
	response.Success(c.Writer, set)
}

func (h *handler) DescribeAction(c *gin.Context) {

	req := action.NewDescribeActionRequest(c.Param("name"), c.Param("version"))

	ins, err := h.service.DescribeAction(c.Request.Context(), req)
	if err != nil {

		response.Failed(c.Writer, err)
	}

	response.Success(c.Writer, ins)
}

func (h *handler) DeleteAction(c *gin.Context) {

	req := action.NewDeleteActionRequest()
	req.Namespace = c.Param("namespace")
	req.Name = c.Param("name")
	req.Version = c.Param("version")

	ins, err := h.service.DeleteAction(c.Request.Context(), req)
	if err != nil {

		response.Failed(c.Writer, err)
	}
	response.Success(c.Writer, ins)

}
