package http

import (
	"gitee.com/talk-about-articles/workflow/api/apps/action"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	h = &handler{}
)

type handler struct {
	service action.ServiceServer
	log     logger.Logger
}

func (h *handler) Config() error {
	h.log = zap.L().Named(action.AppName)
	h.service = app.GetGinApp(action.AppName).(action.ServiceServer)
	return nil
}

func (h *handler) Name() string {
	return action.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(router gin.IRouter) {

	router.POST("/", h.CreateAction)
	router.GET("/:name&:namespace", h.DescribeAction)
	router.GET("/", h.QueryAction)
	router.DELETE("/:name&:namespace&:version/delete", h.DeleteAction)
}

func init() {
	app.RegistryGinApp(h)
}
