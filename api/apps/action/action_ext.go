package action

import (
	"fmt"
	"gitee.com/talk-about-articles/workflow/common/page"
	"github.com/go-playground/validator/v10"
	"github.com/rs/xid"
	"time"
)

var (
	validate = validator.New()
)

func NewActionSet() *ActionSet {

	return &ActionSet{
		Items: []*Action{},
	}

}

func (c *CreateActionRequest) Validate() error {
	return validate.Struct(c)
}

func NewAction(req *CreateActionRequest) (*Action, error) {
	if err := req.Validate(); err != nil {

		return nil, err

	}
	a := &Action{
		Id:           xid.New().String(),
		Logo:         req.Logo,
		DisplayName:  req.DisplayName,
		IsLatest:     true,
		Name:         req.Name,
		Version:      req.Version,
		Domain:       req.Domain,
		Namespace:    req.Namespace,
		CreateAt:     time.Now().UnixMilli(),
		CreateBy:     req.CreateBy,
		UpdateAt:     time.Now().UnixMilli(),
		RunnerType:   req.RunnerType,
		RunnerParams: req.RunnerParams,
		RunParams:    req.RunParams,
		Tags:         req.Tags,
		Description:  req.Description,
	}
	return a, nil
}

func NewDefaultAction() *Action {

	return &Action{}

}

func (s *ActionSet) Add(item *Action) {

	s.Items = append(s.Items, item)

}

func NewQueryActionRequest() *QueryActionRequest {

	return &QueryActionRequest{Page: page.NewDefaultPageRequest()}

}

func NewDescribeActionRequest(name, version string) *DescribeActionRequest {

	return &DescribeActionRequest{

		Id:      xid.New().String(),
		Name:    name,
		Version: version,
	}
}

func (u *UpdateActionRequest) Validate() error {

	return validate.Struct(u)
}

func (d *DeleteActionRequest) Validate() error {

	return validate.Struct(d)
}

func (d *DescribeActionRequest) Validate() error {

	return validate.Struct(d)

}
func (q *QueryActionRequest) Validate() error {

	return validate.Struct(q)

}

func (a *Action) Update(req *UpdateActionRequest) {

	//a.Namespace = req.Name
	a.RunParams = req.RunParams
	a.Tags = req.Tags
	//a.Version = req.Version
	a.Description = req.Description

}

func (a *Action) Key() string {

	return fmt.Sprintf("%s%s", a.Name, a.Version)

}

func NewCreateActionRequest() *CreateActionRequest {

	return &CreateActionRequest{}

}

func (req *CreateActionRequest) UpdateOwner() {

	req.Domain = "admin"
	req.Namespace = "default"
	req.CreateBy = "admin"

}

func NewDeleteActionRequest() *DeleteActionRequest {

	return &DeleteActionRequest{}

}
