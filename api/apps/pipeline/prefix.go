package pipeline

import (
	"fmt"
	"gitee.com/talk-about-articles/workflow/conf"
	"gitee.com/talk-about-articles/workflow/version"
)

// 放入Etcd里面的所有 都需要对key 设计

// resource key define: config_prefix/service_name/resource_type/namespace/resource_name
// course/workflow/pipelines/default/pipeline@v1  pipelines_object_json
func PipelineObjectKey(namespace, id string) string {

	return fmt.Sprintf("%s/%s/%s", EtcdPipelinePrefix(), namespace, id)

}

// course/workflow/steps/default/step@v1  step_object_json
func StepObjectKey(key string) string {

	return fmt.Sprintf("%s/%s", EtcdStepPrefix(), key)
}

// course/workflow/actions/default/bulid_go@v1  action_object_json
func ActionObjectKey(namespace, name, version string) string {

	return fmt.Sprintf("%s/%s/%s@%s", EtcdActionPrefix(), namespace, name, version)

}

func EtcdActionPrefix() string {

	return fmt.Sprintf("%s/%s/Action", conf.C().Etcd.Prefix, version.ServiceName)
}

func EtcdPipelinePrefix() string {

	return fmt.Sprintf("%s/%s/Pipeline", conf.C().Etcd.Prefix, version.ServiceName)
}

func EtcdStepPrefix() string {

	return fmt.Sprintf("%s/%s/Step", conf.C().Etcd.Prefix, version.ServiceName)
}
