package pipeline

import (
	"encoding/json"
	"fmt"
	"gitee.com/talk-about-articles/workflow/common/page"
	"strings"
	"time"
)

// LoadStepFromBytes 解析etcd 的step数据
func LoadStepFromBytes(value []byte) (*Step, error) {
	step := NewDefaultStep()

	// 解析Value
	if len(value) > 0 {
		if err := json.Unmarshal(value, step); err != nil {
			return nil, fmt.Errorf("unmarshal step error, vaule(%s) %s", string(value), err)
		}
	}

	// 校验合法性
	if err := step.Validate(); err != nil {
		return nil, err
	}

	return step, nil
}

// NewStepSet todo
func NewStepSet() *StepSet {
	return &StepSet{
		Items: []*Step{},
	}
}

func (s *StepSet) Add(item *Step) {
	s.Items = append(s.Items, item)
}

func NewStep(t STEP_CREATE_BY, req *CreateStepRequest) *Step {
	return &Step{
		CreateType:   t,
		CreateAt:     time.Now().UnixMilli(),
		Name:         req.Name,
		Action:       req.Action,
		WithAudit:    req.WithAudit,
		AuditParams:  req.AuditParams,
		With:         req.With,
		WithNotify:   req.WithNotify,
		NotifyParams: req.NotifyParams,
		Webhooks:     req.Webhooks,
		NodeSelector: req.NodeSelector,
		Status:       NewDefaultStepStatus(),
	}
}

func NewDefaultStep() *Step {
	return &Step{
		CreateAt: time.Now().UnixMilli(),
		Status:   NewDefaultStepStatus(),
		Webhooks: []*WebHook{},
	}
}

func (s *Step) Validate() error {

	return validate.Struct(s)

}
func (s *DeleteStepRequest) Validate() error {

	return validate.Struct(s)

}

func (c *CreateStepRequest) Validate() error {
	return validate.Struct(c)
}

func (s *Step) MakeObjectKey() string {
	return StepObjectKey(s.Key)
}

func NewDefaultStepStatus() *StepStatus {

	return &StepStatus{

		Response: map[string]string{},
	}
}

func (s *Stage) StepCount() int {

	return len(s.Steps)
}

// 方便阅读的Stage的名称描述
func (s *Stage) ShortDesc() string {

	return fmt.Sprintf("%s[%d]", s.Name, s.Id)

}

func (s *Step) ActionName() string {
	parseArr := s.parseAction()
	return parseArr[0]
}

func (s *Step) ActionVersion() string {
	parseArr := s.parseAction()
	if len(parseArr) > 1 {
		return parseArr[1]
	}
	return ""
}

// pipeline 定义Action 是一个 action_name@action_version
func (s *Step) parseAction() []string {

	return strings.Split(s.Action, "@")

}

func (s *Step) Audit(resp AUDIT_RESPONSE, message string) {

	s.Status.AuditAt = time.Now().UnixMilli()
	s.Status.AuditResponse = resp
	s.Status.AuditMessage = message

	// 如果是审核通过，需要调整step的状态，调整到step的状态为等待执行
	// step controller 才会去执行step
	if s.Status.AuditResponse.Equal(AUDIT_RESPONSE_ALLOW) {
		// 审核初始状态为等待执行状态
		s.Status.Status = STEP_STATUS_PENDDING

	}

}
func (s *Step) Cancel(format string, B ...interface{}) {

	s.Status.Status = STEP_STATUS_CANCELED
	s.Status.Message = fmt.Sprintf(format, B...)

}

func NewDescribeStepRequestWithKey(key string) *DescribeStepRequest {

	return &DescribeStepRequest{

		Key: key,
	}

}

func NewDeleteStepRequestWithKey(key string) *DeleteStepRequest {

	return &DeleteStepRequest{
		Key: key,
	}

}

func NewQueryStepRequest() *QueryStepRequest {

	return &QueryStepRequest{
		Page: &page.PageRequest{},
	}

}

func NewCreateStepRequest() *CreateStepRequest {

	return &CreateStepRequest{}

}

func NewAuditStepRequest() *AuditStepRequest {

	return &AuditStepRequest{}

}

func NewCancelStepRequest() *CancelStepRequest {

	return &CancelStepRequest{}

}
