package http

import (
	"gitee.com/talk-about-articles/workflow/api/apps/pipeline"
	"gitee.com/talk-about-articles/workflow/common/page"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/http/response"
)

func (h *handler) CreateStep(c *gin.Context) {

	req := pipeline.NewCreateStepRequest()
	if err := c.Bind(req); err != nil {

		response.Failed(c.Writer, err)
		return

	}

	req.Namespace = "default"

	ins, err := h.service.CreateStep(c.Request.Context(), req)
	if err != nil {
		response.Failed(c.Writer, err)
		return
	}

	response.Success(c.Writer, ins)

}

func (h *handler) QueryStep(c *gin.Context) {

	page := page.NewPageRequestFromHTTP(c.Request)
	req := pipeline.NewQueryStepRequest()
	req.Page = page

	set, err := h.service.QueryStep(c.Request.Context(), req)
	if err != nil {
		response.Failed(c.Writer, err)
	}
	response.Success(c.Writer, set)
}

func (h *handler) DescribeStep(c *gin.Context) {

	req := pipeline.NewDescribeStepRequestWithKey(c.Param("key"))
	req.Namespace = "default"

	ins, err := h.service.DescribeStep(c.Request.Context(), req)
	if err != nil {

		response.Failed(c.Writer, err)
	}

	response.Success(c.Writer, ins)
}

func (h *handler) DeleteStep(c *gin.Context) {

	req := pipeline.NewDeleteStepRequestWithKey(c.Param("Key"))

	ins, err := h.service.DeleteStep(c.Request.Context(), req)
	if err != nil {

		response.Failed(c.Writer, err)
	}
	response.Success(c.Writer, ins)

}

func (h *handler) AuditStep(c *gin.Context) {

	req := pipeline.NewAuditStepRequest()

	if err := c.Bind(req); err != nil {

		response.Failed(c.Writer, err)
		return

	}

	ins, err := h.service.AuditStep(c.Request.Context(), req)
	if err != nil {

		response.Failed(c.Writer, err)
	}
	response.Success(c.Writer, ins)

}

func (h *handler) CancelStep(c *gin.Context) {

	req := pipeline.NewCancelStepRequest()

	if err := c.Bind(req); err != nil {

		response.Failed(c.Writer, err)
		return

	}

	ins, err := h.service.CancelStep(c.Request.Context(), req)
	if err != nil {

		response.Failed(c.Writer, err)
	}
	response.Success(c.Writer, ins)

}
