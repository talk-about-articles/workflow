package http

import (
	"gitee.com/talk-about-articles/workflow/api/apps/pipeline"
	"gitee.com/talk-about-articles/workflow/common/page"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/http/response"
)

func (h *handler) CreatePipeline(c *gin.Context) {

	req := pipeline.NewCreatePipelineRequest()
	if err := c.BindJSON(req); err != nil {

		response.Failed(c.Writer, err)
		return

	}

	req.UpdateOwner()

	ins, err := h.service.CreatePipeline(c.Request.Context(), req)
	if err != nil {
		response.Failed(c.Writer, err)
		return
	}

	response.Success(c.Writer, ins)

}

func (h *handler) QueryPipeline(c *gin.Context) {

	page := page.NewPageRequestFromHTTP(c.Request)
	req := pipeline.NewQueryPipelineRequest()
	req.Page = page

	set, err := h.service.QueryPipeline(c.Request.Context(), req)
	if err != nil {
		response.Failed(c.Writer, err)
	}
	response.Success(c.Writer, set)
}

func (h *handler) DescribePipeline(c *gin.Context) {

	req := pipeline.NewDescribePipelineRequestWithId(c.Param("id"))
	req.Namespace = "default"

	ins, err := h.service.DescribePipeline(c.Request.Context(), req)
	if err != nil {

		response.Failed(c.Writer, err)
	}

	response.Success(c.Writer, ins)
}

func (h *handler) DeletePipeline(c *gin.Context) {

	req := pipeline.NewDeletePipelineRequestWithID(c.Param("id"))

	ins, err := h.service.DeletePipeline(c.Request.Context(), req)
	if err != nil {

		response.Failed(c.Writer, err)
	}
	response.Success(c.Writer, ins)

}

//func (h *handler) CancelPipeline(c *gin.Context) {
//
//
//	req := pipeline.NewCancelWatchPipelineRequest()
//
//	if err := c.BindJSON(req); err != nil {
//		response.Failed(c.Writer, err)
//	}
//
//	ins, err := h.service.Can(c.Request.Context(), req)
//	if err != nil {
//
//		response.Failed(c.Writer, err)
//	}
//	response.Success(c.Writer, ins)
//}
