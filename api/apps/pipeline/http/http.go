package http

import (
	"gitee.com/talk-about-articles/workflow/api/apps/pipeline"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	api = &handler{}
)

type handler struct {
	service pipeline.ServiceServer
	log     logger.Logger
	//proxy   *proxy
}

func (h *handler) Config() error {

	h.log = zap.L().Named(pipeline.AppName)
	h.service = app.GetGinApp(pipeline.AppName).(pipeline.ServiceServer)

	return nil
}

func (h *handler) Name() string {

	return pipeline.AppName

}

func (h *handler) Version() string {

	return "v1"

}

func (h *handler) Registry(router gin.IRouter) {

	router.POST("/", h.CreatePipeline)
	router.GET("/", h.QueryPipeline)
	router.GET("/:id", h.DescribePipeline)
	router.DELETE("/:id", h.DeletePipeline)

	s := router.Group("step")
	s.POST("/", h.CreateStep)
	s.GET("/", h.QueryStep)
	s.GET("/:key", h.DescribeStep)
	s.DELETE("/:key", h.DeleteStep)
	s.POST("/:key/audit", h.AuditStep)
	s.POST("/:key/cancel", h.CancelStep)

}

func init() {

	app.RegistryGinApp(api)

}
