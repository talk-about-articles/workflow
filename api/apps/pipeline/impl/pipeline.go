package impl

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/talk-about-articles/workflow/api/apps/pipeline"
	"github.com/infraboard/mcube/exception"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// pipeline存入etcd的逻辑
func (s *service) CreatePipeline(ctx context.Context, req *pipeline.CreatePipelineRequest) (*pipeline.Pipeline, error) {

	p, err := pipeline.NewPipeline(req)
	if err != nil {
		return nil, err
	}

	if err := s.validatePipelineStage(ctx, p); err != nil {
		return nil, err

	}

	value, err := json.Marshal(p)
	if err != nil {
		return nil, err
	}
	objKey := p.MakeObjectKey()
	objValue := string(value)

	_, err = s.client.Put(ctx, objKey, objValue)
	if err != nil {
		return nil, fmt.Errorf("put pipeline with key: %s,error,%s", objKey, err)
	}
	s.log.Debugf("create pipeline success,key： %s", objKey)
	return p, nil

}
func (s *service) QueryPipeline(ctx context.Context, req *pipeline.QueryPipelineRequest) (*pipeline.PipelineSet, error) {

	// etctl get --with-prefix key_prefix
	// key prefix: course/workflow/pipelines

	listKey := pipeline.EtcdPipelinePrefix()
	s.log.Info("list etcd pipeline resource key; %s", listKey)
	resp, err := s.client.Get(ctx, listKey, clientv3.WithPrefix())
	if err != nil {
		return nil, err
	}
	ps := pipeline.NewPipelineSet()
	for index := range resp.Kvs {

		//
		ins, err := pipeline.LoadPipelineFromBytes(resp.Kvs[index].Value)
		if err != nil {
			s.log.Error(err)
			continue
		}
		ins.ResourceVersion = resp.Header.Revision
		ps.Add(ins)

	}
	return ps, nil

}
func (s *service) DescribePipeline(ctx context.Context, req *pipeline.DescribePipelineRequest) (*pipeline.Pipeline, error) {

	DesKey := pipeline.PipelineObjectKey(req.Namespace, req.Id)
	s.log.Infof("describe etcd pipeline resource key: %s", DesKey)
	resp, err := s.client.Get(ctx, DesKey)
	if err != nil {
		return nil, err
	}

	if resp.Count == 0 {

		return nil, exception.NewNotFound("pipeline %s not found", req.Id)

	}

	if resp.Count > 1 {

		return nil, exception.NewInternalServerError("pipeline find more than one: %d", resp.Count)

	}

	ins := pipeline.NewDefaultPipeline()
	for index := range resp.Kvs {

		//
		ins, err = pipeline.LoadPipelineFromBytes(resp.Kvs[index].Value)
		if err != nil {
			s.log.Error(err)
			continue
		}
		ins.ResourceVersion = resp.Header.Revision

	}
	return ins, nil

}
func (s *service) WatchPipeline(req pipeline.Service_WatchPipelineServer) error {
	return status.Errorf(codes.Unimplemented, "method WatchPipeline not implemented")
}

// 删除pipeline
func (s *service) DeletePipeline(ctx context.Context, req *pipeline.DeletePipelineRequest) (*pipeline.Pipeline, error) {

	des := pipeline.NewDescribePipelineRequestWithId(req.Id)
	des.Namespace = req.Namespace
	ins, err := s.DescribePipeline(ctx, des)
	if err != nil {
		return nil, err
	}

	// 先删除pipeline对应的step
	if err := s.DeletePipelineStep(ctx, ins); err != nil {
		s.log.Errorf("delete pipeline [%s] steps error,%s", err)
	}

	// 再删除pipeline对象
	descKey := ins.MakeObjectKey()

	s.log.Infof("delete etcd pipeline resource  key :%s", descKey)
	_, err = s.client.Delete(ctx, descKey, clientv3.WithPrefix())
	if err != nil {
		return nil, err
	}
	return ins, nil

}

func (s *service) DeletePipelineStep(ctx context.Context, ins *pipeline.Pipeline) error {

	prefix := ins.StepPrefix()
	if len(prefix) < 12 {
		return fmt.Errorf("prefix length must large than 12")
	}

	deletePrefixKey := pipeline.StepObjectKey(prefix)
	s.log.Infof("delete etcd step resource key prefix: %s", deletePrefixKey)

	resp, err := s.client.Delete(ctx, deletePrefixKey, clientv3.WithPrefix())
	if err != nil {
		return err
	}

	s.log.Infof("delete pipeline %s total %d step", ins.ShortDescribe(), resp.Deleted)

	return nil

}
