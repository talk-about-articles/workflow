package impl

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/talk-about-articles/workflow/api/apps/pipeline"
	"github.com/infraboard/mcube/exception"
	"github.com/rs/xid"
	clientv3 "go.etcd.io/etcd/client/v3"
)

func (s *service) CreateStep(ctx context.Context, req *pipeline.CreateStepRequest) (*pipeline.Step, error) {
	// Step请求校验
	if err := req.Validate(); err != nil {
		return nil, exception.NewBadRequest("validate create step request error, %s", err)
	}
	// 构造Step
	step := pipeline.NewStep(pipeline.STEP_CREATE_BY_PIPELINE, req)
	step.Key = xid.New().String()
	// 校验Step, 需要校验Action是否存在
	if err := s.validateStep(ctx, step); err != nil {
		return nil, exception.NewBadRequest("validate step error, %s", err)
	}

	value, err := json.Marshal(step)
	if err != nil {
		return nil, err
	}
	objKey := step.MakeObjectKey()
	objValue := string(value)

	_, err = s.client.Put(ctx, objKey, objValue)
	if err != nil {
		return nil, fmt.Errorf("put step with key: %s,error,%s", objKey, err)
	}
	s.log.Debugf("create pipeline success,key： %s", objKey)
	return step, nil

}
func (s *service) QueryStep(ctx context.Context, req *pipeline.QueryStepRequest) (*pipeline.StepSet, error) {

	listKey := pipeline.EtcdStepPrefix()
	s.log.Info("list etcd step resource key; %s", listKey)
	resp, err := s.client.Get(ctx, listKey, clientv3.WithPrefix())
	if err != nil {
		return nil, err
	}
	ps := pipeline.NewStepSet()
	for index := range resp.Kvs {

		//
		ins, err := pipeline.LoadStepFromBytes(resp.Kvs[index].Value)
		if err != nil {
			s.log.Error(err)
			continue
		}
		ins.ResourceVersion = resp.Header.Revision
		ps.Add(ins)

	}
	return ps, nil
}
func (s *service) DescribeStep(ctx context.Context, req *pipeline.DescribeStepRequest) (*pipeline.Step, error) {

	DesKey := pipeline.StepObjectKey(req.Key)
	s.log.Infof("describe etcd step resource key: %s", DesKey)
	resp, err := s.client.Get(ctx, DesKey)
	if err != nil {
		return nil, err
	}

	if resp.Count == 0 {

		return nil, exception.NewNotFound("step %s not found", req.Key)

	}

	if resp.Count > 1 {

		return nil, exception.NewInternalServerError("step find more than one: %d", resp.Count)

	}

	ins := pipeline.NewDefaultStep()
	for index := range resp.Kvs {

		//
		ins, err = pipeline.LoadStepFromBytes(resp.Kvs[index].Value)
		if err != nil {
			s.log.Error(err)
			continue
		}
	}
	return ins, nil

}
func (s *service) DeleteStep(ctx context.Context, req *pipeline.DeleteStepRequest) (*pipeline.Step, error) {

	// 校验参数
	if err := req.Validate(); err != nil {
		return nil, exception.NewBadRequest("validate delete request error,%s", err)
	}

	// 删除 step 的时候，返回当前删除的key和value
	desKey := pipeline.StepObjectKey(req.Key)
	s.log.Infof("delete etcd step resource key: %s", desKey)
	resp, err := s.client.Delete(ctx, desKey, clientv3.WithPrefix())
	if err != nil {
		return nil, err
	}

	if resp.Deleted == 0 {

		return nil, exception.NewNotFound("step %s not found", req.Key)

	}

	ins := pipeline.NewDefaultStep()
	for index := range resp.PrevKvs {

		//
		ins, err := pipeline.LoadStepFromBytes(resp.PrevKvs[index].Value)
		if err != nil {
			s.log.Error(err)
			continue
		}
		ins.ResourceVersion = resp.Header.Revision
	}

	return ins, nil

}

// 取消执行
func (s *service) CancelStep(ctx context.Context, req *pipeline.CancelStepRequest) (*pipeline.Step, error) {

	// 查询当前的step
	step, err := s.DescribeStep(ctx, pipeline.NewDescribeStepRequestWithKey(req.Key))
	if err != nil {
		return nil, err
	}

	step.Cancel("cancel step by user")
	// 更新对应的为cancel
	if err := s.putStep(ctx, step); err != nil {

		return nil, fmt.Errorf("update step error,%s", err)
	}
	return step, nil

}
func (s *service) AuditStep(ctx context.Context, req *pipeline.AuditStepRequest) (*pipeline.Step, error) {

	//	1、获取对象
	step, err := s.DescribeStep(ctx, pipeline.NewDescribeStepRequestWithKey(req.Key))
	if err != nil {

		return nil, err

	}
	//  2、判断对象是否开启了审核
	//  如果没有开启审核就直接退出
	if !step.WithAudit {

		return nil, exception.NewBadRequest("this step needn't audit")

	}

	//  3、
	step.Audit(req.AuditReponse, req.AuditMessage)
	if err := s.putStep(ctx, step); err != nil {

		return nil, fmt.Errorf("update step error,%s", err)
	}
	return step, nil
}

// 重新执行一次put操作，覆盖操作
func (s *service) putStep(ctx context.Context, ins *pipeline.Step) error {

	// 等于是重新赋值
	value, err := json.Marshal(ins)
	if err != nil {
		return err
	}

	objKey := ins.MakeObjectKey()
	objValue := string(value)

	if _, err := s.client.Put(ctx, objKey, objValue); err != nil {

		return fmt.Errorf("put step with key: %s,error,%s", objKey, err)

	}
	s.log.Debugf("put step success,key: %s", objKey)

	return nil
}
