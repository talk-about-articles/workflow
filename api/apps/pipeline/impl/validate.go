package impl

import (
	"context"
	"fmt"
	"gitee.com/talk-about-articles/workflow/api/apps/action"
	"gitee.com/talk-about-articles/workflow/api/apps/pipeline"
)

// 校验Pipeline 是否合法
// Stage 校验 是否有Step
// Step 校验, Step 传递的action 不存在, 不是我们提前定义, 也是不合法的step
func (i *service) validatePipelineStage(ctx context.Context, p *pipeline.Pipeline) error {
	for index := range p.Stages {
		stage := p.Stages[index]
		if err := i.validateStage(ctx, stage); err != nil {
			return err
		}
	}

	return nil
}

func (i *service) validateStage(ctx context.Context, s *pipeline.Stage) error {
	if s.StepCount() == 0 {
		return fmt.Errorf("stage %s host no steps", s.ShortDesc())
	}

	for index := range s.Steps {
		step := s.Steps[index]
		if err := i.validateStep(ctx, step); err != nil {
			return err
		}
	}

	return nil
}

func (i *service) validateStep(ctx context.Context, s *pipeline.Step) error {
	// 获取当前step中, action的名称和版本
	// 通过Action模块查询该action是否存在
	_, err := i.action.DescribeAction(ctx, action.NewDescribeActionRequest(s.ActionName(), s.ActionVersion()))
	if err != nil {
		return err
	}
	return nil
}
