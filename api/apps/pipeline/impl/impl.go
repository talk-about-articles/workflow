package impl

import (
	"context"
	"gitee.com/talk-about-articles/workflow/api/apps/action"
	"gitee.com/talk-about-articles/workflow/api/apps/pipeline"
	"gitee.com/talk-about-articles/workflow/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/grpc"
	"sync"
)

var (
	// Service 服务实例
	svr = &service{

		watchCancel: make(map[int64]context.CancelFunc),
	}
)

type service struct {
	// etcd客户端
	client *clientv3.Client
	log    logger.Logger

	pipeline.UnimplementedServiceServer
	// Pipeline中断
	watchCancel map[int64]context.CancelFunc
	// 当前pipeline编号
	currentNumber int64
	l             sync.Mutex
	action        action.ServiceServer
}

func (s *service) SetWatcherCancelFn(fn context.CancelFunc) int64 {

	s.l.Lock()
	defer s.l.Unlock()
	s.currentNumber++
	s.watchCancel[s.currentNumber] = fn
	return s.currentNumber
}

func (s *service) Config() error {
	s.log = zap.L().Named("Pipeline")
	s.client = conf.C().Etcd.GetClient()
	s.action = app.GetGrpcApp(action.AppName).(action.ServiceServer)

	return nil
}

func (s *service) Name() string {
	return pipeline.AppName
}

func (s *service) Debug(log logger.Logger) {
	s.log = log

}

func (s *service) Registry(server *grpc.Server) {
	pipeline.RegisterServiceServer(server, svr)
}

func init() {
	app.RegistryGrpcApp(svr)
}
