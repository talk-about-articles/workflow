package pipeline

import (
	"encoding/json"
	"fmt"
	"gitee.com/talk-about-articles/workflow/common/page"
	"github.com/rs/xid"
	"time"
)

func (c *CreatePipelineRequest) EnsureStep() {

	for m := range c.Stages {

		s := c.Stages[m]
		s.Id = int32(m) + 1
		for n := range s.Steps {
			t := s.Steps[n]
			t.Id = int32(n) + 1
			t.Status = NewDefaultStepStatus()
		}
	}

}

func (c *CreatePipelineRequest) Validate() error {

	return validate.Struct(c)

}

func (req *CreatePipelineRequest) UpdateOwner() {

	req.Domain = "admin"
	req.Namespace = "default"
	req.CreateBy = "admin"

}
func NewDescribePipelineRequestWithId(id string) *DescribePipelineRequest {

	return &DescribePipelineRequest{
		Id: id,
	}

}

func NewDeletePipelineRequestWithID(id string) *DeletePipelineRequest {

	return &DeletePipelineRequest{
		Id: id,
	}

}

func NewPipelineSet() *PipelineSet {

	return &PipelineSet{

		Items: []*Pipeline{},
	}
}

func NewDefaultPipeline() *Pipeline {

	return &Pipeline{

		Status: &PipelineStatus{},
	}
}

func (s *Pipeline) validate() error {

	return validate.Struct(s)

}

func NewPipeline(req *CreatePipelineRequest) (*Pipeline, error) {

	if err := req.Validate(); err != nil {

		return nil, err

	}
	p := &Pipeline{
		Id: xid.New().String(),
		//ResourceVersion: 0,
		Domain:      req.Domain,
		Namespace:   req.Namespace,
		CreateAt:    time.Now().UnixMilli(),
		CreateBy:    req.CreateBy,
		TemplateId:  req.TemplateId,
		Name:        req.Name,
		With:        req.With,
		Tags:        req.Tags,
		Description: req.Description,
		On:          req.On,
		HookEvent:   req.HookEvent,
		//Status:          req.sta,
		Stages: req.Stages,
		Status: &PipelineStatus{},
	}

	return p, nil
}

func (p *Pipeline) EtcdObjectKey() string {

	return fmt.Sprintf("%s/%s/%s", EtcdPipelinePrefix(), p.Namespace, p.Id)

}

func (p *Pipeline) StepPrefix() string {

	return fmt.Sprintf("%s.%s", p.Namespace, p.Id)

}

func (p *Pipeline) ShortDescribe() string {

	return fmt.Sprintf("%s.%s", p.Name, p.Id)

}

// 构建etcd对应的key
// 例如： inforboard/workflow/service/node/node@01
func (p *Pipeline) MakeObjectKey() string {

	return fmt.Sprintf("%s/%s/%s", EtcdPipelinePrefix(), p.Namespace, p.Id)

}

func LoadPipelineFromBytes(data []byte) (*Pipeline, error) {

	pipeline := NewDefaultPipeline()
	if err := json.Unmarshal(data, pipeline); err != nil {
		return nil, err
	}
	if err := pipeline.validate(); err != nil {

		return nil, err
	}

	return pipeline, nil

}

func (s *PipelineSet) Add(item *Pipeline) {

	s.Items = append(s.Items, item)

}

func NewQueryPipelineRequest() *QueryPipelineRequest {

	return &QueryPipelineRequest{
		Page: &page.PageRequest{},
	}

}

func NewCancelWatchPipelineRequest() *CancelWatchPipelineRequest {

	return &CancelWatchPipelineRequest{}
}

func NewCreatePipelineRequest() *CreatePipelineRequest {

	return &CreatePipelineRequest{}

}
