package pipeline

import (
	"github.com/go-playground/validator/v10"
)

const (
	AppName = "pipeline"
)

var (
	validate = validator.New()
)
