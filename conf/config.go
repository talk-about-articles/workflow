package conf

import (
	"context"
	"fmt"
	"github.com/infraboard/mcube/logger/zap"
	clientv3 "go.etcd.io/etcd/client/v3"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"sync"
	"time"
)

var (
	mgoclient  *mongo.Client
	etcdClient *clientv3.Client
)

func newConfig() *Config {
	return &Config{
		App:   newDefaultAPP(),
		Log:   newDefaultLog(),
		Mongo: newDefaultMongoDB(),
		//Redis: newDefaultRedis(),
		HTTP: newDefaultHTTP(),
		GRPC: newDefaultGRPC(),
		Etcd: newDefaultEtcd(),
	}
}

// Config 应用配置
type Config struct {
	App   *app     `toml:"app"`
	Log   *log     `toml:"log"`
	Mongo *mongodb `toml:"mongodb"`
	//Redis *Redis   `toml:"redis"`
	HTTP *http `toml:"http"`
	GRPC *grpc `toml:"grpc"`
	Etcd *Etcd `toml:"etcd"'`
}

func (c *Config) InitGlobal() error {
	// 加载全局配置单例
	global = c
	// 提前加载好 mcenter客户端, 全局变量
	//err := rpc.LoadClientFromConfig(c.Mcenter)
	//if err != nil {
	//	return fmt.Errorf("load mcenter client from config error: " + err.Error())
	//}
	// mcenter 客户端对象就初始化好了
	return nil
}

type app struct {
	Name       string `toml:"name" env:"APP_NAME"`
	EncryptKey string `toml:"encrypt_key" env:"APP_ENCRYPT_KEY"`
	Platform   string `toml:"platform" env:"APP_PLATFORM"`
}

func newDefaultAPP() *app {
	return &app{
		Name:       "workflow",
		EncryptKey: "defualt app encrypt key",
	}
}

type http struct {
	Host      string `toml:"host" env:"HTTP_HOST"`
	Port      string `toml:"port" env:"HTTP_PORT"`
	EnableSSL bool   `toml:"enable_ssl" env:"HTTP_ENABLE_SSL"`
	CertFile  string `toml:"cert_file" env:"HTTP_CERT_FILE"`
	KeyFile   string `toml:"key_file" env:"HTTP_KEY_FILE"`
}

func (a *http) Addr() string {
	return a.Host + ":" + a.Port
}

func newDefaultHTTP() *http {
	return &http{
		Host: "127.0.0.1",
		Port: "8050",
	}
}

type grpc struct {
	Host      string `toml:"host" env:"GRPC_HOST"`
	Port      string `toml:"port" env:"GRPC_PORT"`
	EnableSSL bool   `toml:"enable_ssl" env:"GRPC_ENABLE_SSL"`
	CertFile  string `toml:"cert_file" env:"GRPC_CERT_FILE"`
	KeyFile   string `toml:"key_file" env:"GRPC_KEY_FILE"`
}

func (a *grpc) Addr() string {
	return a.Host + ":" + a.Port
}

func newDefaultGRPC() *grpc {
	return &grpc{
		Host: "127.0.0.1",
		Port: "18050",
	}
}

type log struct {
	Level   string    `toml:"level" env:"LOG_LEVEL"`
	PathDir string    `toml:"path_dir" env:"LOG_PATH_DIR"`
	Format  LogFormat `toml:"format" env:"LOG_FORMAT"`
	To      LogTo     `toml:"to" env:"LOG_TO"`
}

func newDefaultLog() *log {
	return &log{
		Level:   "debug",
		PathDir: "logs",
		Format:  "text",
		To:      "stdout",
	}
}

//func newDefaultRedis() *Redis {
//	return &Redis{
//		Endpoints: []string{"127.0.0.1:6379"},
//		DB:        0,
//		Password:  "",
//	}
//}

//type Redis struct {
//	Endpoints []string `toml:"redis_endpoint" env:"redis_endpoint" envSeparator:","`
//	UserName  string   `toml:"redis_username" env:"redis_username"`
//	Password  string   `toml:"redis_password" env:"redis_password"`
//	DB        int      `toml:"redis_db" env:"redis_db"`
//	lock      sync.Mutex
//}

//// Client 获取一个全局的mongodb客户端连接
//func (r *Redis) Client() (*redis.Client, error) {
//	// 加载全局数据量单例
//	r.lock.Lock()
//	defer r.lock.Unlock()
//	if redisclient == nil {
//		conn, err := r.getClient()
//		if err != nil {
//			return nil, err
//		}
//		redisclient = conn
//	}
//
//	return redisclient, nil
//}

//func (r *Redis) GetDB() (*redis.Client, error) {
//	conn, err := r.Client()
//	if err != nil {
//		return nil, err
//	}
//	return conn, nil
//}

//func (r *Redis) getClient() (*redis.Client, error) {
//
//	defaultClient := newDefaultRedis()
//
//	client := redis.NewClient(&redis.Options{
//		Addr:     defaultClient.Endpoints[0],
//		Password: defaultClient.Password,
//		DB:       defaultClient.DB,
//	})
//
//	// Connect to MongoDB
//	//client, err := mongo.Connect(context.TODO(), opts)
//	//if err != nil {
//	//	return nil, fmt.Errorf("new mongodb client error, %s", err)
//	//}
//	//
//	//if err = client.Ping(context.TODO(), nil); err != nil {
//	//	return nil, fmt.Errorf("ping mongodb server(%s) error, %s", m.Endpoints, err)
//	//}
//
//	return client, nil
//}

func newDefaultMongoDB() *mongodb {
	return &mongodb{
		Database:  "",
		Endpoints: []string{"127.0.0.1:27017"},
	}
}

type mongodb struct {
	Endpoints []string `toml:"endpoint" env:"MONGO_ENDPOINTS" envSeparator:","`
	UserName  string   `toml:"username" env:"MONGO_USERNAME"`
	Password  string   `toml:"password" env:"MONGO_PASSWORD"`
	Database  string   `toml:"database" env:"MONGO_DATABASE"`
	lock      sync.Mutex
}

// Client 获取一个全局的mongodb客户端连接
func (m *mongodb) Client() (*mongo.Client, error) {
	// 加载全局数据量单例
	m.lock.Lock()
	defer m.lock.Unlock()
	if mgoclient == nil {
		conn, err := m.getClient()
		if err != nil {
			return nil, err
		}
		mgoclient = conn
	}

	return mgoclient, nil
}

func (m *mongodb) GetDB() (*mongo.Database, error) {
	conn, err := m.Client()
	if err != nil {
		return nil, err
	}
	return conn.Database(m.Database), nil
}

func (m *mongodb) getClient() (*mongo.Client, error) {
	opts := options.Client()

	cred := options.Credential{
		AuthSource: m.Database,
	}

	if m.UserName != "" && m.Password != "" {
		cred.Username = m.UserName
		cred.Password = m.Password
		cred.PasswordSet = true
		opts.SetAuth(cred)
	}
	opts.SetHosts(m.Endpoints)
	opts.SetConnectTimeout(5 * time.Second)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), opts)
	if err != nil {
		return nil, fmt.Errorf("new mongodb client error, %s", err)
	}

	if err = client.Ping(context.TODO(), nil); err != nil {
		return nil, fmt.Errorf("ping mongodb server(%s) error, %s", m.Endpoints, err)
	}

	return client, nil
}

func newDefaultEtcd() *Etcd {
	return &Etcd{
		InstanceTTL: 300,
		Prefix:      "tzdemo",
	}
}

type Etcd struct {
	Endpoints   []string `toml:"endpoints" env:"ETCD_ENDPOINTS" envSeparator:","`
	UserName    string   `toml:"username" env:"ETCD_USERNAME"`
	Password    string   `toml:"password" env:"ETCD_PASSWORD"`
	Prefix      string   `toml:"prefix" env:"ETCD_Prefix"`
	InstanceTTL int64    `toml:"instance_ttl" env:"ETCD_INSTANCE_TTL"`
}

func (e *Etcd) Validate() error {
	if len(e.Endpoints) == 0 {
		return fmt.Errorf("etcd enpoints not config")
	}
	return nil
}

func (e *Etcd) GetClient() *clientv3.Client {
	if etcdClient == nil {
		panic("please load etcd client first")
	}

	return etcdClient
}

func (e *Etcd) getClient() (*clientv3.Client, error) {
	timeout := time.Duration(5) * time.Second
	client, err := clientv3.New(clientv3.Config{
		Endpoints:   e.Endpoints,
		DialTimeout: timeout,
		Username:    e.UserName,
		Password:    e.Password,
	})
	if err != nil {
		return nil, fmt.Errorf("connect etcd error, %s", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	ml, err := client.MemberList(ctx)
	if err != nil {
		return nil, err
	}
	zap.L().Debugf("etcd members: %s", ml.Members)

	return client, nil
}
