package conf

import (
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

var (
	global *Config
)

// C 全局配置对象
func C() *Config {
	if global == nil {
		panic("Load Config first")
	}
	return global
}

// 初始化全局实例, etcd client, mongo client
func initGloabalInstance(cfg *Config) error {
	c, err := cfg.Etcd.getClient()
	if err != nil {
		return err
	}
	etcdClient = c

	mgo, err := cfg.Mongo.getClient()
	if err != nil {
		return err
	}
	mgoclient = mgo
	return nil
}

// LoadConfigFromToml 从toml中添加配置文件, 并初始化全局对象
func LoadConfigFromToml(filePath string) error {
	cfg := newConfig()
	if _, err := toml.DecodeFile(filePath, cfg); err != nil {
		return err
	}
	// 加载全局配置单例

	if err := initGloabalInstance(cfg); err != nil {
		return err
	}

	//fmt.Println(cfg)
	//	 加载全局单例
	return cfg.InitGlobal()
}

// LoadConfigFromEnv 从环境变量中加载配置
func LoadConfigFromEnv() error {
	cfg := newConfig()
	if err := env.Parse(cfg); err != nil {
		return err
	}
	// 加载全局配置单例
	return cfg.InitGlobal()

}
